#include <iostream>

using namespace std;


class Engine
{
    private:
        int *arr;
        int size;
    
    public:
        Engine(int a[] , int s)
        {
            arr  = a;
            size = s;
        }
    
        int findSunsetViewBuildings()
        {
            int count   = 0;
            int tallest = 0;
            for(int i = 0; i < size ; i++)
            {
                if(tallest < arr[i])
                {
                    tallest = arr[i];
                    count++;
                }
            }
            return count;
        }
};

int main(int argc, const char * argv[])
{
    int arr[] = {2, 3, 4, 5};
    int size  = (int)(sizeof(arr)/sizeof(arr[0]));
    Engine e  = Engine(arr , size);
    cout<<e.findSunsetViewBuildings()<<endl;
    return 0;
}
